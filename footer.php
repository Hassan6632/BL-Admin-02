<!--    [ Start Footer Area]-->
<footer>
    <div class="copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="copy-content">
                        <p>&copy; 2018. All Rights Reserved. Theme By <span>PreneurLAB.com</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--    [Finish Footer Area]-->

<!--SCROLL TOP BUTTON-->
<a href="#" class="top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<!--    [jQuery]-->
<script src="assets/js/jquery-3.2.1.min.js"></script>

<!--    [Popper Js] -->
<script src="assets/js/popper.min.js"></script>

<!--    [Bootstrap Js] -->
<script src="assets/js/bootstrap.min.js"></script>

<!--    [OwlCarousel Js]-->
<script src="assets/js/owl.carousel.min.js"></script>

<!--    [Navbar Fixed Js] -->
<script src="assets/js/navbar-fixed.js"></script>

<!--    [Main Custom Js] -->
<script src="assets/js/main.js"></script>



<script>
    $(document).ready(function() {
        $("#tgl-btn").click(function() {
            $("#main-menu").toggle("slow");
        });
    });

</script>

</body>

</html>
