<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Title</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/logo.png">


</head>

<body>



    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->

        <!-- 
2 - Navbar with brand left, links on center and right that collapse in the vertical mobile
    menu
-->
        <div id="main-menu" class="main-menu">
            <nav class="navbar navbar-light navbar-expand-md bg-light justify-content-between">
                <a href="index.php" class="navbar-brand">
                    <div class="bl-logo  text-center">
                        <div class="logo"><img src="assets/img/logo.png" alt=""></div>
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar2"><span class="navbar-toggler-icon"></span></button>
                <div class="navbar-collapse collapse justify-content-between" id="collapsingNavbar2">
                    <div>
                        <!--placeholder to evenly space flexbox items and center links-->
                    </div>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">DASHBOARD </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">QUERY</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">NEW QUERY</a>
                                <a class="dropdown-item" href="#">MENAGE QUERY</a>
                                <a class="dropdown-item" href="#">QUERY DASHBOARD</a>

                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CAMPAIGNS</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">DAILY CAMPAIGNS</a>
                                <a class="dropdown-item" href="#">WEEKLY CAMPAIGNS</a>
                                <a class="dropdown-item" href="#">MONTHLY CAMPAIGNS</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ADDS</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SETTINGS </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">ADD ADMIN</a>
                                <a class="dropdown-item" href="#">ADD ACCOUNT</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">INBOX/ MESSAGE </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">HELP/INQUIRY </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav flex-row">

                    </ul>
                </div>
            </nav>
        </div>


        <div id="tgl-btn"><i class="icofont icofont-justify-all"></i></div>


        <!-- 
3 - Navbar with brand center, links on left and right that all collapse into the vertical mobile
    menu
-->
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
