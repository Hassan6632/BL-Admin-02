<?php include ('header.php');?>
<!--    [ Strat Section Title Area]-->
<section id="help" class="body-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="help-title text-center">
                    <h4>Contact To Preneur Lab</h4>
                </div>
                <div class="hrlp-contact">
                    <form action="">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" placeholder="Name">
                                <input type="email" placeholder="Email" required>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" placeholder="Subject">
                                <input type="number" min="7" max="13" placeholder="Phone">
                            </div>
                        </div>
                        <textarea name="" id=""></textarea>
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                                <div class="submit-btn text-center">
                                    <button>SUBMIT</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!--    [Finish Section Title Area]-->
<?php include ('footer.php');?>
