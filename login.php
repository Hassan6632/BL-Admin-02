<?php include 'header.php';?>
<style>
    header,
    footer {
        display: none;
    }

</style>
<!--    [ Strat Section Title Area]-->
<section id="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="login-title text-center">
                    <h3>Admin Pannel</h3>
                </div>

                <form action="">
                    <div class="log-form text-center">
                        <div class="log-logo">
                            <img src="assets/img/logo.png" alt="">
                        </div>
                        <div class="log-tbl">
                            <div class="log-in-style text-left">
                                <i class="icofont icofont-user-male"></i>
                                <i class="icofont icofont-lock"></i>
                            </div>
                            <input type="text" name="user" placeholder="Username">
                            <input type="password" name="pass" placeholder="Password">
                        </div>
                        <a href="#" class="log-footer">
                            <h4>Sign In</h4>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->
<?php include 'footer.php';?>
