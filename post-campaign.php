<?php include ('header.php');?>
<!--    [ Strat Section Title Area]-->
<section id="query" class="body-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="page-title text-center">
                    <h3>Weekly Campaigns</h3>
                </div>
            </div>
        </div>

        <form action="">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="post-ques">
                        <input type="text" name="title" class="form-control" id="exampleInputEmail1" placeholder="Enter title or question">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="post-campaign-sub-title text-center">
                                <h4>Input Payloads</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="post-camp-input">
                        <div class="post-camp-input">
                            <input type="text" name="pay1" class="form-control" id="exampleInputEmail1" placeholder="Payload 1">
                            <input type="text" name="pay2" class="form-control" id="exampleInputEmail1" placeholder="Payload 2">
                            <input type="text" name="pay3" class="form-control" id="exampleInputEmail1" placeholder="Payload 3">
                            <input type="text" name="pay4" class="form-control" id="exampleInputEmail1" placeholder="Payload 4">
                            <input type="text" name="pay5" class="form-control" id="exampleInputEmail1" placeholder="Payload 5">
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="post-campaign-sub-title">
                                    <h4>Add A Time Schedule:</h4>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="post-campaign-sub-title">
                                    <h4>Demography:</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="post-time-day">
                                    <input type="time" name="mytime">

                                </div>
                            </div>
                            <div class="col-lg-4">
                                <select name="day">
                                <option value="Sunday">Sunday</option>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednessday">Wednessday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturnday">Saturnday</option>
                            </select>
                            </div>
                            <div class="col-lg-4">
                                <div class="post-demo">
                                    <select name="demography">
                            <option value="All User">All User</option>
                        <!--    <option value="Male">Male</option>
                            <option value="Female">Female</option> -->
                            
                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="post-camp-input">
                        <input type="text" name="pay1" class="form-control" id="exampleInputEmail1" placeholder="Payload 6">
                        <input type="text" name="pay2" class="form-control" id="exampleInputEmail1" placeholder="Payload 7">
                        <input type="text" name="pay3" class="form-control" id="exampleInputEmail1" placeholder="Payload 8">
                        <input type="text" name="pay4" class="form-control" id="exampleInputEmail1" placeholder="Payload 9">
                        <input type="text" name="pay5" class="form-control" id="exampleInputEmail1" placeholder="Payload 10">
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="post-campaign-sub-title">
                                <h4>File input</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="post-file-input">
                                <input type="file" id="exampleInputFile" name="files">
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="submit-btn">
                                <button>SUBMIT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<!--    [Finish Section Title Area]-->
<?php include ('footer.php');?>
