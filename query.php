<?php include ('header.php');?>
<!--    [ Strat Section Title Area]-->
<section id="summery" class="body-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="page-title text-center">
                    <h3>Query</h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <a href="new-query.php" class="query-info text-center">
                    <i class="icofont icofont-paperclip"></i>
                    <h4>New Query</h4>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="" class="query-info text-center">
                    <i class="icofont icofont-cement-mix"></i>
                    <h4>Update Query</h4>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="" class="query-info text-center">
                    <i class="icofont icofont-ui-settings"></i>
                    <h4>Query Panel</h4>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="" class="query-info text-center">
                    <i class="icofont icofont-play-alt-3"></i>
                    <h4>Query Control Tutorial</h4>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="" class="query-info text-center">
                    <i class="icofont icofont-monitor"></i>
                    <h4>New Carousel</h4>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="" class="query-info text-center">
                    <i class="icofont icofont-settings"></i>
                    <h4>Menagement Carousel</h4>
                </a>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->
<?php include ('footer.php');?>
