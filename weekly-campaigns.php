<?php include ('header.php');?>
<!--    [ Strat Section Title Area]-->
<section id="summery" class="body-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="page-title text-center">
                    <h3>Weekly Campaigns</h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <a href="post-campaign.php" class="query-info text-center">
                    <i class="icofont icofont-hand-drag1"></i>
                    <h4>Post Campaign</h4>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="" class="query-info text-center">
                    <i class="icofont icofont-under-construction"></i>
                    <h4>Campaign Control Panel</h4>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="" class="query-info text-center">
                    <i class="icofont icofont-chart-flow-alt-1"></i>
                    <h4>Campaign Analytics</h4>
                </a>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->
<?php include ('footer.php');?>
